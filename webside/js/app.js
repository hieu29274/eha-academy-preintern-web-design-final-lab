
function onClick(element) {
    document.getElementById("img01").src = element.src;
    document.getElementById("modal01").style.display = "block";
    var captionText = document.getElementById("caption");
    captionText.innerHTML = element.alt;
}



var mySidebar = document.getElementById("mySidebar");

function w3_open() {
    if (mySidebar.style.display === 'block') {
        mySidebar.style.display = 'none';
    } else {
        mySidebar.style.display = 'block';
    }
}


function w3_close() {
    mySidebar.style.display = "none";
}

function validateName() {
    let name = document.getElementById("name").value;
    let errorName = document.getElementById("errorName")
    let regexName = /^([a-zA-Z]{2,}\s[a-zA-Z]{1,}'?-?[a-zA-Z]{2,}\s?([a-zA-Z]{1,})?)/;
    if (name == "" || name == null) {
        errorName.innerHTML = "Please enter your name";
    } else if (!regexName.test(name)) {
        errorName.innerHTML = "Please enter a valid name";
    } else {
        errorName.innerHTML = "";
    }
    return name
}
function validateEmail() {

    let email = document.getElementById("email").value;
    let errorEmail = document.getElementById("errorEmail")
    let regexEmail = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    if (email == "" || email == null) {
        errorEmail.innerHTML = "Please enter your email";
    } else if (!regexEmail.test(email)) {
        errorEmail.innerHTML = "Please enter a valid email";
    } else {
        errorEmail.innerHTML = "";
    }
    return email;
}
function validate() {
        alert("Done!!!");
}